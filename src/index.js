const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const route = require('./routers');
const morgan = require('morgan');
const db = require('./utils/db');

///////////////////
const app = express();
dotenv.config();
//config PORT
const PORT = process.env.PORT || 8080;

//middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(morgan('dev'));

//server run
app.listen(PORT, () => {
  console.log(`listening on port http://localhost:${PORT}`);
  //router init
  route(app);
  // connect db
  db.connection();
});
