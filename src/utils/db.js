const mongoose = require('mongoose');

async function connection() {
  try {
    await mongoose.connect(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log('connected to database ');
  } catch (error) {
    console.error(error);
    console.log('could not connected to database ');
    process.exit(1);
  }
}
module.exports = { connection };
