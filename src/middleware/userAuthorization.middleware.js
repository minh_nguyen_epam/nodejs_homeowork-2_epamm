const jwt = require('jsonwebtoken');
const User = require('./../models/user.model');

exports.validUserToken = (req, res, next) => {
  console.log('.src/middleware/userAuthorization.middleware.js/validUserToken');
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (!token) {
    return res.status(401).json({ message: 'Please send token' });
  }
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.status(403).json({ message: 'Invalid token' });
    }
    req.user = user;
    req.accessToken = token;
    next();
  });
};
exports.validUser = async (req, res, next) => {
  console.log('.src/middleware/userAuthorization.middleware.js/validUserToken');
  const { _id } = req.user;
  try {
    const user = await User.findById(_id).select('+password');
    if (!user) {
      return res.status(400).json({ message: 'User doesnt exist' });
    }
    req.user = user;
    next();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
