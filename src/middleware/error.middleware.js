exports.errorHandler = (err, req, res, next) => {
  if (err.status >= 400 || err.status <= 499) {
    return res.status(err.status).json(err);
  }
  return res.status(500).json({ status: 500, message: error.message });
};
