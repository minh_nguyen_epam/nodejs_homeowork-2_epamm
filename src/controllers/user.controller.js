// const authUtils = require('./../utils/auth');
const User = require('../models/user.model');
const _ = require('lodash');
const bcrypt = require('bcrypt');

exports.getUserProfile = async (req, res, next) => {
  res.json(req.user);
};

exports.deleteUserProfile = async (req, res, next) => {
  try {
    // console.log('asdasdasd', req.user);
    const deletedUser = await User.findOneAndDelete(req.user._id);
    if (!deletedUser) {
      throw { status: 400, message: 'User not exist' };
    }
    const user = _.omit(deletedUser.toJSON(), 'password');
    res.status(400).json({ deletedUser: user, message: 'success' });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.changeUserPassword = async (req, res, next) => {
  console.log('aaaaa');
  try {
    const { oldPassword, newPassword } = req.body;
    const user = req.user;
    const isSame = await bcrypt.compare(oldPassword, user.password);
    if (!isSame) {
      throw { status: 400, message: 'Your password is not correct' };
    }

    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(newPassword, salt);

    const updated = await User.findByIdAndUpdate(
      user._id,
      { password: hashedPassword },
      { new: true },
    );
    return res.json({ message: 'Sucess' });
  } catch (error) {
    next(error);
  }
};
