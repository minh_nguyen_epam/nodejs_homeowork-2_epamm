// const authUtils = require('./../utils/auth');
const User = require('../models/user.model');
const Session = require('../models/session.model');
const bcrypt = require('bcrypt');
const {
  USERNAME_LENGTH_MIN,
  USERNAME_LENGTH_MAX,
  PASSWORD_LENGTH_MIN,
  PASSWORD_LENGTH_MAX,
} = require('./../constants/form');
const {
  generateAccessToken,
  generateRefreshToken,
} = require('./../utils/auth.utils');
const _ = require('lodash');
exports.register = async (req, res, next) => {
  console.log('src/controllers/auth.controller.js/register');
  try {
    const { username, password } = req.body;
    if (!username || !password) {
      throw {
        status: 400,
        message: 'username and password are required',
      };
    }

    if (
      username.length < USERNAME_LENGTH_MIN ||
      username.length > USERNAME_LENGTH_MAX
    ) {
      throw {
        status: 400,
        message: `username must be between ${USERNAME_LENGTH_MIN}, ${USERNAME_LENGTH_MAX} characters`,
      };
    }
    if (
      password.length < PASSWORD_LENGTH_MIN ||
      password.length > PASSWORD_LENGTH_MAX
    ) {
      throw {
        status: 400,
        message: `password must between ${PASSWORD_LENGTH_MIN}, ${PASSWORD_LENGTH_MAX} characters`,
      };
    }

    if (await User.findOne({ username: username })) {
      throw {
        status: 400,
        message: `username ${username} already exist.`,
      };
    }
    //////////save database
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);
    const userFull = await new User({
      username,
      password: hashedPassword,
    }).save();
    const user = _.omit(userFull.toJSON(), 'password');
    const accessToken = generateAccessToken({
      _id: user._id,
      username: user.username,
    });
    const refreshToken = generateRefreshToken({
      _id: user._id,
      username: user.username,
    });
    await Session.create({
      refreshToken,
      userAgent: req.get('user-agent' || null),
    });

    return res.status(200).json({
      message: 'Success',
      userInfo: user,
      accessToken,
      refreshToken,
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.login = async (req, res, next) => {
  console.log('src/controllers/auth.controller.js/login');
  const { username, password } = req.body;
  try {
    /////////check condition
    if (!username || !password) {
      throw {
        status: 400,
        message: 'username and password are required',
      };
    }

    if (
      username.length < USERNAME_LENGTH_MIN ||
      username.length > USERNAME_LENGTH_MAX
    ) {
      throw {
        status: 400,
        message: `username must between ${USERNAME_LENGTH_MIN}, ${USERNAME_LENGTH_MAX} character`,
      };
    }
    if (
      password.length < PASSWORD_LENGTH_MIN ||
      password.length > PASSWORD_LENGTH_MAX
    ) {
      throw {
        status: 400,
        message: `password must between ${PASSWORD_LENGTH_MIN}, ${PASSWORD_LENGTH_MAX} character`,
      };
    }
    const userFull = await User.findOne({
      username: username,
    }).select('+password');

    if (!userFull) {
      throw { status: 400, error: 'Account does not exist.' };
    }
    const { password: hashedPassword, ...user } = _.omit(userFull.toJSON());
    if (!(await bcrypt.compare(password, hashedPassword))) {
      throw { status: 400, error: 'Wrong password' };
    }
    const accessToken = generateAccessToken({
      _id: user._id,
      username: user.username,
    });
    const refreshToken = generateRefreshToken({
      _id: user._id,
      username: user.username,
    });
    await new Session({
      refreshToken,
      userAgent: req.get('user-agent') || '',
    }).save();
    return res.json({
      user: user,
      accessToken,
      refreshToken,
      message: 'Success',
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

// exports.logout = async (req, res, next) => {
//   console.log('./src/controller/user.controller.js/logout');
//   try {
//     const accessToken = req.accessToken;
//     const deletedSession = await Session.findOneAndDelete({
//       session: accessToken,
//     }).lean();
//     if (!deletedSession) {
//       throw { status: 404, message: 'Phiên đăng nhập hết hạn' };
//     }
//     return res.status(200).json({ message: 'Đăng xuất thành công' });
//   } catch (error) {
//     next(error);
//   }
// };
