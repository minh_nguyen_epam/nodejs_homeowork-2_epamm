const Note = require('../models/note.model');
const User = require('../models/user.model');

exports.createNote = async (req, res) => {
  try {
    const createdNote = await Note.create({
      ...req.body,
      userId: req.user._id,
    });
    res.status(200).json({ success: 'Success', note: createdNote });
  } catch (err) {
    res.status(500).json(err);
  }
};

exports.allNoteOfAnUser = async (req, res) => {
  try {
    const allNotes = await Note.find({ userId: req.user._id });
    res.status(200).json(allNotes);
  } catch (error) {
    res.status(500).json(error);
  }
};

exports.getNoteById = async (req, res, next) => {
  try {
    console.log('a');
    console.log(req.params.id);
    const note = await Note.findById(req.params.id).exec();
    if (!note) {
      throw { status: 400, message: 'This note is not exist anymore' };
    }
    res.json({ note: note });
  } catch (error) {
    res.status(400).json({ message: 'This note no longer exist' });
  }
};

exports.updateNoteById = async (req, res, next) => {
  try {
    const updatedNote = await Note.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res.json({ message: 'Success', updatedNote });
  } catch (error) {
    return next(error);
  }
};
exports.checkNote = async (req, res, next) => {
  try {
    Note.findOne({ _id: req.params.id }, function (err, note) {
      note.completed = !note.completed;
      note.save(function (err, updatedNote) {
        console.log(updatedNote);
        res.json({ message: 'Success', updatedNote });
      });
    });
  } catch (error) {
    return next(error);
  }
};
exports.deleteNote = async (req, res, next) => {
  try {
    const deletedNote = await Note.findByIdAndDelete(req.params.id);
    if (!deletedNote) {
      throw { status: 400, message: 'Note no longer exist' };
    }
    res.json({ message: 'Success', deletedNote });
  } catch (error) {
    next(error);
  }
};
