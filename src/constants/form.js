exports.USERNAME_LENGTH_MIN = 6;
exports.USERNAME_LENGTH_MAX = 30;
exports.PASSWORD_LENGTH_MIN = 6;
exports.PASSWORD_LENGTH_MAX = 30;
