const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SessionSchema = new Schema(
  {
    refreshToken: {
      type: String,
      required: true,
    },
    userAgent: { type: String, default: '' },
  },
  { timestamps: true },
);

module.exports = mongoose.model('Session', SessionSchema);
