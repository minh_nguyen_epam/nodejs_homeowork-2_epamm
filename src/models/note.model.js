const mongoose = require('mongoose');

const NoteSchema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    completed: { type: Boolean, default: false },

    text: { type: String, required: true },
  },
  { timestamps: true },
);
console.log('src/models/note.model.js');
module.exports = mongoose.model('Note', NoteSchema);
