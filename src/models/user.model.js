const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema(
  {
    username: { type: String, required: true, min: 6, max: 40 },
    password: { type: String, select: false, required: true },
  },
  { timestamps: true },
);
console.log('src/model/user.model.js');

module.exports = mongoose.model('User', UserSchema);
