const userRouter = require('express').Router();
const {
  getUserProfile,
  deleteUserProfile,
  changeUserPassword,
} = require('../controllers/user.controller');
const {
  validUserToken,
  validUser,
} = require('./../middleware/userAuthorization.middleware');

userRouter.get('/me', validUserToken, validUser, getUserProfile);
userRouter.delete('/me', validUserToken, deleteUserProfile);
userRouter.patch('/me', validUserToken, validUser, changeUserPassword);
module.exports = userRouter;
