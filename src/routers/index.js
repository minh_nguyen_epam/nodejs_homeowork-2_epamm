//khai báo các routes
const userRouter = require('./user.router');
const noteRouter = require('./note.router');
const authRouter = require('./auth.router');
const { errorHandler } = require('../middleware/error.middleware');

//body
function route(app) {
  //auth
  app.use('/api/auth', authRouter);
  //note
  app.use('/api/notes', noteRouter);
  //user
  app.use('/api/user', userRouter);

  app.use(errorHandler);
  //index
  app.use('/', (req, res) => {
    res.send('xin chao');
  });
}
module.exports = route;
