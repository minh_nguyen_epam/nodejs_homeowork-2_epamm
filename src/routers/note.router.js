const router = require('express').Router();
const {
  createNote,
  allNoteOfAnUser,
  getNoteById,
  updateNoteById,
  checkNote,
  deleteNote,
} = require('../controllers/note.controller');
const {
  validUserToken,
} = require('../middleware/userAuthorization.middleware');

router.post('/', validUserToken, createNote);
router.get('/', validUserToken, allNoteOfAnUser);
router.get('/:id', validUserToken, getNoteById);
router.put('/:id', validUserToken, updateNoteById);
router.patch('/:id', validUserToken, checkNote);
router.delete('/:id', validUserToken, deleteNote);

module.exports = router;
